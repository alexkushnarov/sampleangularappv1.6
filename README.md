## Test App
### Requirements
- Node.js ~6.9.x
- NPM

### Get started
```bash
# install dependencies
npm install

# spin up a webserver for development
npm start
```
